/*
* On HTML5 DOM Load Function
*/
window.onload = function(){ 


    init();

    // Set the event Listener
    document.getElementById('fileinput').addEventListener('change', readSingleFile, false); 
}

/*
* Read File From Input
*/
function readSingleFile(evt) {
    //Retrieve the first (and only!) File from the FileList object
    var f = evt.target.files[0]; 

    if (f) {
      var r = new FileReader();
      r.onload = function(e) { 
      	  var contents = JSON.parse(e.target.result); 		 // Full Parsed Content
      	  lines = contents.lines;
      	  circles = contents.circles;
      	  curves = contents.curves;
	      var temp_points = [];

	      drawObjects(); // -- > After Setting the objetcs, we can draw them all 
	      calculateCenterObject(); // calculate the Center of Object()



	      // Iterate over the points array to draw the image
	      /*i = 0;
	      while(i<points.length) {
	      	drawLine(points[i], points[i+1]);
	      	i= i+2; // skip 2 points ahead
	      } */

      }
      r.readAsText(f);
    } else { 
      alert("Failed to load file");
    }
  }

  /*
  * drawObjects Function
  * Draw All Parsed Objects
  * Will Be called again after transformations
  */
  function drawObjects() {
  	// Iterate over the lines array to draw
      $.each( lines, function( key, value ) {
      	drawLine(value.p1, value.p2);
	});
      // Iterate over the circles array to draw
      $.each( circles, function( key, circle ) {
      	drawCircle(circle);
	});

      // Iterate over the circles array to draw
      $.each( curves, function( key, curve ) {
      	drawCurve(curve);
	});


  }







/*
* Global Variables
*/
var canvas,
    context,
    points = [],
    form_values = [],
    dragging = false,
    dragStartLocation,
    dragStopLocation,
    snapshot,
    curve_pick = false,
    curveArray = [],
    operation_element,
    operation,
    lines = [],
    circles = [],
    curves = [],
    colored_li,
    fillBox,

    xMax = 0,
    yMax = 0,
    xMin = 100000, 	/*"curves": [{"p1":{"x": 40,"y": 50},"p2":{"x": 100,"y": 50},"p3":{"x": 150,"y": 150}, "p4":{"x": 200,"y": 200}}] */

    yMin = 100000,

    xCenterObject,
    yCenterObject;



    /*
	* Draw Line Function
	*/
	function drawLine(p1, p2) {
		var x_from  = p1.x1 , y_from = p1.y1;
		var x_to = p2.x2, y_to = p2.y2 ;

		if(x_from == null) {
			x_from  = p1.x , y_from = p1.y;
			x_to = p2.x, y_to = p2.y ;
		}
		

		// If we are going negative way
		
		if(x_to < x_from) {
			var temp_x = x_to;
			x_to = x_from;
			x_from = temp_x;
			var temp_y = y_to;
			y_to = y_from;
			y_from = temp_y;
		}

		if(y_to < y_from) {
			var temp_y = y_to;
			y_to = y_from;
			y_from = temp_y;
		}

		var dx = x_to - x_from;
		var dy = y_to - y_from;
		var error = 0;
		var derror = Math.abs(dy/dx);
		var y = y_from;

		/* if x_from ==x_to -- > run only on y */
		if(x_from == x_to) {
			var x = x_from;
			console.log('both x are the same');
			for(var y = y_from; y <= y_to; y++) {
				putPixel(x, y);
			}
			return;
		}
		
		for(var x = x_from; x <= x_to; x++) {
			//var y = y_to + dy * (x - x_from) / dx;
			putPixel(x, y);
			error = error + derror;
			while(error >= 0.5) {
				putPixel(x, y);
				y = y + Math.sign(y_to - y_from);
				error = error - 1.0;
			}
		}
	}

	 
    /*
    * Bresenaim Circle Function
    */
	function bres_circle ( x,  y,  circle) {
		var x_center = circle.x, y_center = circle.y;
		putPixel(x_center + x , y_center + y);
		putPixel(x_center - x , y_center + y);
		putPixel(x_center + x , y_center - y);
		putPixel(x_center - x , y_center - y);
		putPixel(x_center + y , y_center + x);
		putPixel(x_center - y , y_center + x);
		putPixel(x_center + y , y_center - x);
		putPixel(x_center - y , y_center - x);
	}

	/*
	* Oוur Basic PutPixel Function
	*/
	function putPixel(x, y) {
		context.fillRect(x, y, 1, 1);
	}


	/*
	* Draw A Circle Function
	*/
	function drawCircle(circle) {
	 	//int radius = 50, x = 0, 
	 	var radius = circle.radius;
	 	var y = radius, p ;
	 	var x = 0;
		p = 3 - 2*radius;
		bres_circle(x, y, circle);
		while(x < y) {
			bres_circle(x, y, circle);
			if(p < 0) {
				p = p + 4 * x + 6;
			}
			else {
				p = p + 4 * (x - y) + 10;
				y--;
			}
			x++;
		}
		while(x < y) {
			if(x == y) {
				bres_circle(x, y, circle);
			}
		}
	}

	/*
	* Draw A Bezier Curve
	*/
	function drawCurve(points) {
		console.log('inside drawCurve !');

		/* Bezier Points Calculations */
		var bezier = function(t, p0, p1, p2, p3){
		      	var dx =  p0.x,
					cx = -3 * p0.x +3 * p1.x,
					bx =  3 * p0.x -6 * p1.x +3 * p2.x,
					ax = -p0.x +3 * p1.x -3 * p2.x + p3.x;

				var dy =  p0.y,
					cy = -3 * p0.y +3 * p1.y,
					by =  3 * p0.y -6 * p1.y +3 * p2.y,
					ay = -p0.y +3 * p1.y -3 * p2.y + p3.y;	
		            
		      var x = (ax * Math.pow(t,3)) + (bx * Math.pow(t,2)) + (cx * t) + dx;
		      var y = (ay * Math.pow(t,3)) + (by * Math.pow(t,2)) + (cy * t) + dy;
		            
		      return {x: x, y: y};
    	}

    	  var accuracy = 0.1;
    	  dragStartLocation = points[0]; // Our Starting Point
    	  var old_p;
    	  for (var i=0; i<1; i+=accuracy) {

	         var p = bezier(i, points.p1, points.p2, points.p3, points.p4);
	         if(i ==0 ) {
	         	old_p = p;
	         }

	         // self additions
	         drawLine(old_p, p);
	         old_p = p;
      		}
	}



	/*
	* Draw Polygon Function
	*/
	function drawPolygon(event) {
		context.save(); // saves the current context stage
		var radius, edges;
		dragStartLocation = getCanvasCoordinates(event); // Set The Middle Point
		/* We Need To Prompt Questions For : Radius, # Of Edges */
		radius = parseInt(prompt("Please enter the radius length"));
		if (radius != null) {
			edges = parseInt(prompt("Please enter number of edges"));
    	}
		
		 if (edges < 3) {
		  	return;
		  } 
		 var a = (Math.PI * 2)/edges;
		 var rotateAngle = -Math.PI/2;
		 context.beginPath();
		 context.translate(dragStartLocation.x,dragStartLocation.y);
		 context.rotate(rotateAngle);
		 context.moveTo(radius,0);
		 for (var i = 1; i < edges; i++) {
		    context.lineTo(radius*Math.cos(a*i),radius*Math.sin(a*i));
		 }
		 context.closePath();
		 context.stroke();

		 context.restore(); // Restores back the latest context stage
	  
}	

	/*
	* Function To Erase The Canvas Platform
	*/
	function eraseCanvas() {
				// Store the current transformation matrix
		context.save();

		// Use the identity matrix while clearing the canvas
		context.setTransform(1, 0, 0, 1, 0, 0);
		/*var canvas = document.getElementById("myCanvas");
		var context = canvas.getContext('2d'); */	
		context.clearRect(0, 0, canvas.width, canvas.height);
	}


/*
* TakeSnapshot
*/
function takeSnapshot() {
    snapshot = context.getImageData(0, 0, canvas.width, canvas.height);
}

/*
* RstoreSnapshot
*/
function restoreSnapshot() {
    context.putImageData(snapshot, 0, 0);
}

/*
* Getting Canvas Coordinates
*/ 
function getCanvasCoordinates(event) {
    var x = event.clientX - canvas.getBoundingClientRect().left,
        y = event.clientY - canvas.getBoundingClientRect().top;

    return {x: x, y: y};
}


/*
* Draw Switch Case Function
*/ 
function draw(position, shape) {
    if (shape === "circle") {
        drawCircle(position);
    }
    if (shape === "line") {
        drawLine(position);
    }
}

/*
*On Start Mouse Click
*/
function dragStart(event) {
    dragging = true;
    dragStartLocation = getCanvasCoordinates(event);
    takeSnapshot();
}

/*
* When Dragging The Mouse
*/ 
function drag(event) {
    var position;
    if (dragging === true) {
        //restoreSnapshot();
        position = getCanvasCoordinates(event);
        //draw(position, "circle");
        // draw(position, shape);

    }
}

/*
* When Releasing The Mouse
*/ 
function dragStop(event) {
	console.log('dragged stop');
    dragging = false;
    //restoreSnapshot();
    dragStopLocation = getCanvasCoordinates(event); // where weve stopped
    operationDetermine();
}

/*
* Used to determine whether the click is for: 
* 1. Bezier Curve
* 2. Regular Polygon
*/
function clickDetermine(event) {

	if(shape == "polygon") { // If its a polygon click
		console.log('polygon was clicked !');
		drawPolygon(event);
		return;
	}
	if (shape == "curve") {
		console.log("direct to curve");
		canvasClicked(event); // Direct to Bezier Curve Drawing Event
	}
	else {
		return;
	}
}

/*
* Used to collect points
*/
function canvasClicked(event) {
	dragStartLocation = getCanvasCoordinates(event);
	// Check if curve is picked
	if(curve_pick) {
		curveArray.push(dragStartLocation);
	}
	if(curveArray.length ==4) {
		console.log('4!');
		drawCurve(curveArray);
		curveArray = []; // zero the  coordinates array
	}

}

/* 
* Init Program Function, Takes part when DOM Is Done
*/
function init() {
    canvas = document.getElementById("myCanvas");
    context = canvas.getContext('2d');
    context.strokeStyle = 'green';
    context.fillStyle = 'red';
    context.lineWidth = 4;
    context.lineCap = 'round';

    /*context.translate(0, 600);
	context.scale(1, -1);*/

    /*
    * Set Selected Image <li> Opacity
    */
    function setOpacity(selected) {
    	/* Set Current Shape Opacity */
    	if(operation_element != null) {
    		operation_element.setAttribute("style", "opacity: 1;"); 
    		operation_element.setAttribute("style", "background-color: white;");
    	}
    	operation_element = selected;
    	operation_element.setAttribute("style", "opacity: 0.6;");
    	operation_element.setAttribute("style", "background-color: beige;");
    }

    /*
    * On Operation Click - Show Fetch Form
    */
    $(".operations_nav li").click(function(){
    	setOpacity(this);
    	var op = (this).id;
    	operation = op;
    	console.log( op + ' clicked');
        $(".fetch_form").show(); // show our Form
    });

    /*
	* Fetch Form Handler
	*/
	$("#submit_from").click(function( event ) {
	  console.log('submit clicked')
	  var input_list = $(".fetch_form :input[type='number']");
	  input_list.each(function(){
		    form_values.push(parseInt(this.value));
	   });
	  console.log(form_values);// weve got the new points
	  operationDetermine();
	});


	/*
	* On Color Change Click
	*/
	$(".colors li").click(function(e){
		var color = $(this).css("background-color");
		context.fillStyle = color; 		// General Color
		context.strokeStyle = color;   // This one is for the Polygons 
		/* Return opacity of old colored li*/ 
		if(colored_li != null) {
			colored_li.style.opacity = 1;
		}
		colored_li = e.target;
		colored_li.style.opacity = 0.5;
	}); 


	/*
	* Mouse Event Listeners
	*/
    //canvas.addEventListener('click', clickDetermine, false);
    canvas.addEventListener('mousedown', dragStart, false);
    canvas.addEventListener('mousemove', drag, false);
    canvas.addEventListener('mouseup', dragStop, false);
    

}

/*
* Calculate new point after delta
*/
function diff(value, delta) {
	if(value > delta) {
		return (value - delta);
	}
	return (value + delta);
}


/*
* Translation Function
* Used to move object to his new position
*/
function translation() {
	 // 1. erase the board
	// 2. get the array of objects
   // 3. for each one, add the delta 
  // 4. redraw the objects 

  // erase board
  eraseCanvas();
  // lines
  $.each(lines, function( key, line ) {

  		line.p1.x1  = diff(line.p1.x1, form_values[0]);
		line.p1.y1  = diff(line.p1.y1, form_values[1]);
		line.p2.x2  = diff(line.p2.x2, form_values[0]);
		line.p2.y2  = diff(line.p2.y2, form_values[1]);

/*
		// check the points of the object for calculating the center of the object

		// check the X of thr first point
		if(value.p1.x1 > xMax) {
	      	xMax = value.p1.x1;
	    }

		if(value.p1.x1 < xMin) {
			xMin = value.p1.x1;
		}


		// check the Y of thr first point
		if(value.p1.y1 > yMax) {
			yMax = value.p1.y1;
		}

		if(value.p1.y1 < yMin) {
			yMin = value.p1.y1;
		}


		// check the X of thr second point
		if(value.p2.x2 > xMax) {
			xMax = value.p2.x2;
		}
		if(value.p2.x2 < xMin) {
			xMin = value.p2.x2;
		}

		// check the Y of thr second point
		if(value.p2.y2 > yMax) {
			yMax = value.p2.y2;
		}
		if(value.p2.y2 < yMin) {
			yMin = value.p2.y2;
		}


		//calculate the center of the object
		xCenterObject = (xMax + xMin) / 2;
    	yCenterObject = (yMax + yMin) / 2;

    	ToCoordinateSystem();
    	*/


	});
  // circles
  $.each(circles, function( key, circle ) {
  		circle.x = diff(circle.x, form_values[0]);
		circle.y  = diff(circle.y, form_values[1]);
	});



  // Redraw
  drawObjects();
  	// Iterate over the points array to redraw the image
	  /*i = 0;
	  while(i<points.length) {
	  	drawLine(points[i], points[i+1]);
	  	i= i+2; // skip 2 points ahead
	  } */


}




/*
* Change the position of the object to the coordinate system
*/
function ToCoordinateSystem() {
  		
  		//line.p1.x1  = diff(line.p1.x1, xCenterObject);
		// line.p1.y1  = diff(line.p1.y1, yCenterObject);
		// line.p2.x2  = diff(line.p2.x2, xCenterObject);
		// line.p2.y2  = diff(line.p2.y2, yCenterObject);


		line.p1.x1  = line.p1.x1 - xCenterObject;
		line.p1.y1  = line.p1.y1 - yCenterObject;
		line.p2.x2  = line.p2.x2 - xCenterObject;
		line.p2.y2  = line.p2.y2 - yCenterObject;
}



function fromCoordinateSystem() {
  		
  		//line.p1.x1  = diff(line.p1.x1, xCenterObject);
		// line.p1.y1  = diff(line.p1.y1, yCenterObject);
		// line.p2.x2  = diff(line.p2.x2, xCenterObject);
		// line.p2.y2  = diff(line.p2.y2, yCenterObject);


		line.p1.x1  = line.p1.x1 + xCenterObject;
		line.p1.y1  = line.p1.y1 + yCenterObject;
		line.p2.x2  = line.p2.x2 + xCenterObject;
		line.p2.y2  = line.p2.y2 + yCenterObject;
}



/*
* check the points of the object for calculating the center of the object
*/
		
function calculateCenterObject() {
  		
		$.each( lines, function( key, line ) { // each value is point

  		// check the X of thr first point
		if(line.p1.x1 > xMax) {
	      	xMax = line.p1.x1;
	    }

		if(line.p1.x1 < xMin) {
			xMin = line.p1.x1;
		}


		// check the Y of thr first point
		if(line.p1.y1 > yMax) {
			yMax = line.p1.y1;
		}

		if(line.p1.y1 < yMin) {
			yMin = line.p1.y1;
		}


		// check the X of thr second point
		if(line.p2.x2 > xMax) {
			xMax = line.p2.x2;
		}
		if(line.p2.x2 < xMin) {
			xMin = line.p2.x2;
		}

		// check the Y of thr second point
		if(line.p2.y2 > yMax) {
			yMax = line.p2.y2;
		}
		if(line.p2.y2 < yMin) {
			yMin = line.p2.y2;
		}



    })

		//calculate the center of the object
		xCenterObject = (xMax + xMin) / 2;
    	yCenterObject = (yMax + yMin) / 2;

    	console.log('center: ' +  xCenterObject + ' ' + yCenterObject) ;
}





/*
* Scaling Function
* Used to rescale our object
* 1. Move every point as delta between the new point
* 2. enlarge/scale it
* 3. Back to new x,y
*/
function scaling() {
	  // 1. gets new position
	 // 2. ask user: Kx, Ky
    // 3. Reset All lines, circles, curves
   // 4. erase canvas 
  // 5. redraw the points 
  //eraseCanvas();
  var K;
		//dragStartLocation = getCanvasCoordinates(event); // Set The Middle Point
		/* We Need To Prompt Questions For : Kx, Ky */
		K = parseInt(prompt("Please enter the new x scaling ratio"));

    	console.log('Kx: ' + K);
    	console.log('start location: ' + dragStartLocation.x + ' ' +dragStartLocation.y);
    	// 1. Calculate New Delta For Each Point
    	$.each( lines, function( key, line ) { // each value is point

  			line.p1.x1  = (line.p1.x1 * K);
			line.p1.y1  = (line.p1.y1 * K);
			line.p2.x2  = (line.p2.x2 * K);
			line.p2.y2  = (line.p2.y2 * K);




			line.p1.x1 =  (line.p1.x1 + dragStartLocation.x - xCenterObject);
			line.p1.y1  = (line.p1.y1 + dragStartLocation.y - yCenterObject);
			line.p2.x2  = (line.p2.x2 + dragStartLocation.x - xCenterObject);
			line.p2.y2  = (line.p2.y2 + dragStartLocation.y - yCenterObject);

			

			console.log('center: ' +  xCenterObject + ' ' + yCenterObject) ;
			console.log('dragStartLocation: ' +  dragStartLocation.x + ' ' + dragStartLocation.y) ;
			console.log('line.p1.x1 : ' +  line.p1.x1) ;
			console.log('line.p1.y1 : ' +  line.p1.y1) ;
			console.log('line.p2.x2 : ' +  line.p2.x2) ;
			console.log('line.p2.y2 : ' +  line.p2.y2) ;


// check the points of the object for calculating the center of the object

		// check the X of thr first point


		});

		$.each( circles, function( key, circle ) { // each value is point
	  			circle.x  = dragStartLocation.x;
				circle.y  = dragStartLocation.y; 
				circle.radius = (circle.radius * K);
		});  
		//calculateCenterObject();

		console.log('xCenterObject: ' +xCenterObject + ' yCenterObject: ' + yCenterObject);

		
    	// Redraw
    	drawObjects();

   xMax = 0;
    yMax = 0;
    xMin = 100000;
    yMin = 100000;


}

/*
* Shearing Function
*/
function shearing() {
	var a = null; // a parameter
	while(a == null) {
		a = parseInt(prompt("Please enter the a parameter:"));
		if ( !Number.isInteger(a)) {
			a = null;
			alert("Please enter correct a parameter");
    	};
	}



  	console.log('a: ' + a);

  // erase board
  //eraseCanvas();
  // lines
  var new_x;
  var canvas_height = canvas.height, canvas_width = canvas.width;

  for(var i=0;i<lines.length;i++) {      // choices for : a  1. delta between trigger mouse and point, 2. promt // circles: like translation, only x
  	lines[i].p1.x1  = (lines[i].p1.x1 + (a*lines[i].p1.y1));   // needs y multiply -1
	lines[i].p2.x2  = (lines[i].p2.x2 + (a*lines[i].p2.y2));  // needs y multiply -1


  }
  // circles
  $.each(circles, function( key, circle ) {
  		circle.x = (circle.x + (a*circle.y));		//diff(circle.x, form_values[0]);
		//circle.y  = 				diff(circle.y, form_values[1]);
	});






  // Redraw
  drawObjects();

  	/*context.translate(0, 600);
	context.scale(1, -1); */
}



/*
* Used to determine whether 2D operation: 
* 1. Translation
* 2. Scaling
* 3. Rotation
*/
function operationDetermine() {
	console.log('inside opertaion determine');
	if(operation == "translation") { // If its a polygon click
		console.log('translation is the action');
		translation();
	}
	if (operation == "scaling") {
		console.log("scaling is the action");
		scaling();
	}
	if(operation == "shearing") {
		console.log("shearing is the action");
		shearing();
	}
	/*else {
		return;
	}*/

	form_values = [];
	return;
}